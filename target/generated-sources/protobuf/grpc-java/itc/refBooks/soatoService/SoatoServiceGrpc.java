package itc.refBooks.soatoService;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.28.1)",
    comments = "Source: itc/refBooks/soatoProtocol/SoatoService.proto")
public final class SoatoServiceGrpc {

  private SoatoServiceGrpc() {}

  public static final String SERVICE_NAME = "itc.refBooks.soatoService.SoatoService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult> getUpsertMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Upsert",
      requestType = itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement.class,
      responseType = itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult> getUpsertMethod() {
    io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement, itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult> getUpsertMethod;
    if ((getUpsertMethod = SoatoServiceGrpc.getUpsertMethod) == null) {
      synchronized (SoatoServiceGrpc.class) {
        if ((getUpsertMethod = SoatoServiceGrpc.getUpsertMethod) == null) {
          SoatoServiceGrpc.getUpsertMethod = getUpsertMethod =
              io.grpc.MethodDescriptor.<itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement, itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Upsert"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult.getDefaultInstance()))
              .setSchemaDescriptor(new SoatoServiceMethodDescriptorSupplier("Upsert"))
              .build();
        }
      }
    }
    return getUpsertMethod;
  }

  private static volatile io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getOneElementMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "OneElement",
      requestType = itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement.class,
      responseType = itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getOneElementMethod() {
    io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement, itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getOneElementMethod;
    if ((getOneElementMethod = SoatoServiceGrpc.getOneElementMethod) == null) {
      synchronized (SoatoServiceGrpc.class) {
        if ((getOneElementMethod = SoatoServiceGrpc.getOneElementMethod) == null) {
          SoatoServiceGrpc.getOneElementMethod = getOneElementMethod =
              io.grpc.MethodDescriptor.<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement, itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "OneElement"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult.getDefaultInstance()))
              .setSchemaDescriptor(new SoatoServiceMethodDescriptorSupplier("OneElement"))
              .build();
        }
      }
    }
    return getOneElementMethod;
  }

  private static volatile io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getTopListMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "TopList",
      requestType = itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList.class,
      responseType = itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getTopListMethod() {
    io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList, itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getTopListMethod;
    if ((getTopListMethod = SoatoServiceGrpc.getTopListMethod) == null) {
      synchronized (SoatoServiceGrpc.class) {
        if ((getTopListMethod = SoatoServiceGrpc.getTopListMethod) == null) {
          SoatoServiceGrpc.getTopListMethod = getTopListMethod =
              io.grpc.MethodDescriptor.<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList, itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "TopList"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult.getDefaultInstance()))
              .setSchemaDescriptor(new SoatoServiceMethodDescriptorSupplier("TopList"))
              .build();
        }
      }
    }
    return getTopListMethod;
  }

  private static volatile io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getChildrenListMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ChildrenList",
      requestType = itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren.class,
      responseType = itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getChildrenListMethod() {
    io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren, itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> getChildrenListMethod;
    if ((getChildrenListMethod = SoatoServiceGrpc.getChildrenListMethod) == null) {
      synchronized (SoatoServiceGrpc.class) {
        if ((getChildrenListMethod = SoatoServiceGrpc.getChildrenListMethod) == null) {
          SoatoServiceGrpc.getChildrenListMethod = getChildrenListMethod =
              io.grpc.MethodDescriptor.<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren, itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ChildrenList"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult.getDefaultInstance()))
              .setSchemaDescriptor(new SoatoServiceMethodDescriptorSupplier("ChildrenList"))
              .build();
        }
      }
    }
    return getChildrenListMethod;
  }

  private static volatile io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult> getAdmHMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "AdmH",
      requestType = itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH.class,
      responseType = itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH,
      itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult> getAdmHMethod() {
    io.grpc.MethodDescriptor<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH, itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult> getAdmHMethod;
    if ((getAdmHMethod = SoatoServiceGrpc.getAdmHMethod) == null) {
      synchronized (SoatoServiceGrpc.class) {
        if ((getAdmHMethod = SoatoServiceGrpc.getAdmHMethod) == null) {
          SoatoServiceGrpc.getAdmHMethod = getAdmHMethod =
              io.grpc.MethodDescriptor.<itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH, itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "AdmH"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult.getDefaultInstance()))
              .setSchemaDescriptor(new SoatoServiceMethodDescriptorSupplier("AdmH"))
              .build();
        }
      }
    }
    return getAdmHMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SoatoServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SoatoServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SoatoServiceStub>() {
        @java.lang.Override
        public SoatoServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SoatoServiceStub(channel, callOptions);
        }
      };
    return SoatoServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SoatoServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SoatoServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SoatoServiceBlockingStub>() {
        @java.lang.Override
        public SoatoServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SoatoServiceBlockingStub(channel, callOptions);
        }
      };
    return SoatoServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SoatoServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SoatoServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SoatoServiceFutureStub>() {
        @java.lang.Override
        public SoatoServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SoatoServiceFutureStub(channel, callOptions);
        }
      };
    return SoatoServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class SoatoServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement> upsert(
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult> responseObserver) {
      return asyncUnimplementedStreamingCall(getUpsertMethod(), responseObserver);
    }

    /**
     */
    public void oneElement(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> responseObserver) {
      asyncUnimplementedUnaryCall(getOneElementMethod(), responseObserver);
    }

    /**
     */
    public void topList(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> responseObserver) {
      asyncUnimplementedUnaryCall(getTopListMethod(), responseObserver);
    }

    /**
     */
    public void childrenList(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> responseObserver) {
      asyncUnimplementedUnaryCall(getChildrenListMethod(), responseObserver);
    }

    /**
     */
    public void admH(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult> responseObserver) {
      asyncUnimplementedUnaryCall(getAdmHMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getUpsertMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement,
                itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult>(
                  this, METHODID_UPSERT)))
          .addMethod(
            getOneElementMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement,
                itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>(
                  this, METHODID_ONE_ELEMENT)))
          .addMethod(
            getTopListMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList,
                itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>(
                  this, METHODID_TOP_LIST)))
          .addMethod(
            getChildrenListMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren,
                itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>(
                  this, METHODID_CHILDREN_LIST)))
          .addMethod(
            getAdmHMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH,
                itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult>(
                  this, METHODID_ADM_H)))
          .build();
    }
  }

  /**
   */
  public static final class SoatoServiceStub extends io.grpc.stub.AbstractAsyncStub<SoatoServiceStub> {
    private SoatoServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SoatoServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SoatoServiceStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoCommands.UpsertSoatoElement> upsert(
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getUpsertMethod(), getCallOptions()), responseObserver);
    }

    /**
     */
    public void oneElement(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getOneElementMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void topList(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getTopListMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void childrenList(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getChildrenListMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void admH(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH request,
        io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAdmHMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SoatoServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<SoatoServiceBlockingStub> {
    private SoatoServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SoatoServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SoatoServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult oneElement(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement request) {
      return blockingUnaryCall(
          getChannel(), getOneElementMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> topList(
        itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList request) {
      return blockingServerStreamingCall(
          getChannel(), getTopListMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> childrenList(
        itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren request) {
      return blockingServerStreamingCall(
          getChannel(), getChildrenListMethod(), getCallOptions(), request);
    }

    /**
     */
    public itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult admH(itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH request) {
      return blockingUnaryCall(
          getChannel(), getAdmHMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SoatoServiceFutureStub extends io.grpc.stub.AbstractFutureStub<SoatoServiceFutureStub> {
    private SoatoServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SoatoServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SoatoServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult> oneElement(
        itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement request) {
      return futureUnaryCall(
          getChannel().newCall(getOneElementMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult> admH(
        itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH request) {
      return futureUnaryCall(
          getChannel().newCall(getAdmHMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_ONE_ELEMENT = 0;
  private static final int METHODID_TOP_LIST = 1;
  private static final int METHODID_CHILDREN_LIST = 2;
  private static final int METHODID_ADM_H = 3;
  private static final int METHODID_UPSERT = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SoatoServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SoatoServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ONE_ELEMENT:
          serviceImpl.oneElement((itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoElement) request,
              (io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>) responseObserver);
          break;
        case METHODID_TOP_LIST:
          serviceImpl.topList((itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoTopList) request,
              (io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>) responseObserver);
          break;
        case METHODID_CHILDREN_LIST:
          serviceImpl.childrenList((itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoChildren) request,
              (io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoElementResult>) responseObserver);
          break;
        case METHODID_ADM_H:
          serviceImpl.admH((itc.refBooks.soatoProtocol.SoatoCommands.GetSoatoAdmH) request,
              (io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoAdmHResult>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_UPSERT:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.upsert(
              (io.grpc.stub.StreamObserver<itc.refBooks.soatoProtocol.SoatoResults.SoatoUpsertResult>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SoatoServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SoatoServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return itc.refBooks.soatoService.SoatoServiceOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SoatoService");
    }
  }

  private static final class SoatoServiceFileDescriptorSupplier
      extends SoatoServiceBaseDescriptorSupplier {
    SoatoServiceFileDescriptorSupplier() {}
  }

  private static final class SoatoServiceMethodDescriptorSupplier
      extends SoatoServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SoatoServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SoatoServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SoatoServiceFileDescriptorSupplier())
              .addMethod(getUpsertMethod())
              .addMethod(getOneElementMethod())
              .addMethod(getTopListMethod())
              .addMethod(getChildrenListMethod())
              .addMethod(getAdmHMethod())
              .build();
        }
      }
    }
    return result;
  }
}

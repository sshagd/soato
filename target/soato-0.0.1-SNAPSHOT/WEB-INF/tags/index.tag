<%--<!DOCTYPE html>--%>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%--<%@tag description="Simple Template" pageEncoding="UTF-8"%>--%>

<%--<%@attribute name="title"%>--%>
<%--<%@attribute name="head" fragment="true" %>--%>
<%--<%@attribute name="filter" fragment="true" %>--%>
<%--<%@attribute name="body" fragment="true" %>--%>

<%--<html>--%>
<%--<head>--%>
    <%--<title>${title}</title>--%>
    <%--<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>--%>

    <%--<link href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css" rel='stylesheet' type="text/css">--%>
    <%--<link href="${pageContext.request.contextPath}/webjars/font-awesome/css/all.css" rel="stylesheet" type="text/css"/>--%>
    <%--<link href="${pageContext.request.contextPath}/webjars/bootstrap-glyphicons/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css"/>--%>
    <%--<link href="${pageContext.request.contextPath}/webjars/Eonasdan-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>--%>
    <%--<link href="${pageContext.request.contextPath}/resources/css/dashboard.css" rel="stylesheet" type="text/css"/>--%>
    <%--<link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet" type="text/css"/>--%>

    <%--<script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>--%>
    <%--<script src="${pageContext.request.contextPath}/webjars/momentjs/min/moment.min.js"></script>--%>
    <%--<script src="${pageContext.request.contextPath}/webjars/momentjs/min/moment-with-locales.min.js"></script>--%>
    <%--<script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.min.js"></script>--%>
    <%--<script src="${pageContext.request.contextPath}/webjars/Eonasdan-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>--%>
    <%--<jsp:invoke fragment="head"/>--%>
<%--</head>--%>
<%--<body>--%>
<%--&lt;%&ndash;<div>&ndash;%&gt;--%>
    <%--&lt;%&ndash;<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow fix-min-width top-menu">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<a class="navbar-brand col-sm-3 col-md-2 mr-0 min-width180" href="#">Мониторинг условий труда</a>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<ul class="navbar-nav px-3 ml-auto">&ndash;%&gt;--%>
            <%--&lt;%&ndash;<li class="nav-item text-nowrap">&ndash;%&gt;--%>
                <%--&lt;%&ndash;<a class="nav-link text-white" href="#">${login}</a>&ndash;%&gt;--%>
            <%--&lt;%&ndash;</li>&ndash;%&gt;--%>
            <%--&lt;%&ndash;<li class="nav-item text-nowrap">&ndash;%&gt;--%>
                <%--&lt;%&ndash;<a class="nav-link" href="${pageContext.request.contextPath}/sign-in/logout">Sign out</a>&ndash;%&gt;--%>
            <%--&lt;%&ndash;</li>&ndash;%&gt;--%>
        <%--&lt;%&ndash;</ul>&ndash;%&gt;--%>
    <%--&lt;%&ndash;</nav>&ndash;%&gt;--%>
<%--&lt;%&ndash;</div>&ndash;%&gt;--%>

<%--&lt;%&ndash;<div class="container-fluid">&ndash;%&gt;--%>
    <%--&lt;%&ndash;<div class="row">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<nav class="col-md-2 d-none d-md-block bg-light sidebar min-width180">&ndash;%&gt;--%>
            <%--&lt;%&ndash;<div class="sidebar-sticky">&ndash;%&gt;--%>
                <%--&lt;%&ndash;<ul class="nav flex-column">&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<li class="nav-item">&ndash;%&gt;--%>
                        <%--&lt;%&ndash;<a class="nav-link ${users}" href="${pageContext.request.contextPath}/user/index">&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<i class="fa fa-users"></i>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;Пользователи <span class="sr-only"></span>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;</a>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;</li>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;<li class="nav-item">&ndash;%&gt;--%>
                        <%--&lt;%&ndash;<a class="nav-link ${organization}" href="${pageContext.request.contextPath}/org/index">&ndash;%&gt;--%>
                            <%--&lt;%&ndash;<i class="fa fa-users"></i>&ndash;%&gt;--%>
                            <%--&lt;%&ndash;Организации <span class="sr-only"></span>&ndash;%&gt;--%>
                        <%--&lt;%&ndash;</a>&ndash;%&gt;--%>
                    <%--&lt;%&ndash;</li>&ndash;%&gt;--%>
                <%--&lt;%&ndash;</ul>&ndash;%&gt;--%>
            <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
        <%--&lt;%&ndash;</nav>&ndash;%&gt;--%>
    <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
<%--&lt;%&ndash;</div>&ndash;%&gt;--%>

<%--<div class="j-modal"></div>--%>

<%--<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">--%>
    <%--<jsp:invoke fragment="filter"/>--%>
    <%--<jsp:invoke fragment="body"/>--%>
<%--</main>--%>
<%--</body>--%>
<%--</html>--%>

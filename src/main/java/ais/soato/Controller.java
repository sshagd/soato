package ais.soato;

import itc.refBooks.soatoProtocol.SoatoResults;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.*;

@org.springframework.stereotype.Controller
public class Controller {
    public Map<Integer, Map<String, String>> regions = new LinkedHashMap<>();

    @RequestMapping("/")
    String start(Model model){
        List<SoatoResults.SoatoElementResult> topList = new GrpcClient().topList();
        Map<String, String> regionMap = new LinkedHashMap<>();

        for (SoatoResults.SoatoElementResult obj: topList) {
            regionMap.put(obj.getElement().getId(), obj.getElement().getLMap().get("RU").getN());
        }

        regions.put(1, regionMap);

        model.addAttribute("regions", regions);
        return "index";
    }

    @RequestMapping(value = "/child", method = RequestMethod.POST)
    String child(@RequestParam String id, Model model){

        int N = 0;
        if(regions.size() > 1) {
            for (int i = 1; i < regions.size(); i++) {
                for (String str : regions.get(i).keySet()) {
                    if (str.equals(id)) {
                        N = i;
                    }
                }
            }
        }

        if(N > 0){
            for(int m = regions.size(); m > N; m--){
                regions.remove(m);
            }
        }

        List<SoatoResults.SoatoElementResult> childList = new GrpcClient().childList(id);
        if(childList.size() > 0) {
            Map<String, String> child = new LinkedHashMap<>();

            for (SoatoResults.SoatoElementResult obj : childList) {
                child.put(obj.getElement().getId(), obj.getElement().getLMap().get("RU").getN());
            }
            regions.put(regions.size() + 1, child);
        }

        model.addAttribute("regions", regions);
        return "index";
    }
}

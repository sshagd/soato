package ais.soato;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import itc.refBooks.soatoProtocol.SoatoCommands;
import itc.refBooks.soatoProtocol.SoatoResults;
import itc.refBooks.soatoService.SoatoServiceGrpc;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GrpcClient {

    public static void main(String[] args) {

    }

    public List<SoatoResults.SoatoElementResult> topList(){
        List<SoatoResults.SoatoElementResult> topList = new ArrayList<>();
        ManagedChannel channel = ManagedChannelBuilder.forAddress("10.64.49.184", 9009)
                .usePlaintext()
                .build();

        SoatoServiceGrpc.SoatoServiceBlockingStub stub
                = SoatoServiceGrpc.newBlockingStub(channel);

        Iterator<SoatoResults.SoatoElementResult> soatoElementResult = stub.topList(SoatoCommands.GetSoatoTopList.newBuilder()
                .build());

        while (soatoElementResult.hasNext()){
            topList.add(soatoElementResult.next());
        }
        return topList;
    }

    public List<SoatoResults.SoatoElementResult> childList(String id){
        List<SoatoResults.SoatoElementResult> childList = new ArrayList<>();
        ManagedChannel channel = ManagedChannelBuilder.forAddress("10.64.49.184", 9009)
                .usePlaintext()
                .build();

        SoatoServiceGrpc.SoatoServiceBlockingStub stub
                = SoatoServiceGrpc.newBlockingStub(channel);

        Iterator<SoatoResults.SoatoElementResult> soatoElementResult = stub.childrenList(SoatoCommands.GetSoatoChildren.newBuilder()
                .setContext(SoatoCommands.SoatoContext.None)
                .setAdmHId(id)
                .build());
        while (soatoElementResult.hasNext()){
            childList.add(soatoElementResult.next());
        }
        return childList;
    }
}

package ais.soato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoatoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoatoApplication.class, args);
	}

}

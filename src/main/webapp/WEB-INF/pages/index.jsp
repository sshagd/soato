<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
    <head>
        <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/grpc.js"></script>
        <link href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css" rel='stylesheet' type="text/css">
    </head>

    <body>
        <c:forEach var="item" items="${regions}">
            <div id="soato" class="input-group col-6">
                <select style="margin: 10px" class="custom-select">
                    <c:forEach var="region" items="${item.value}">
                        <option data-id="${region.key}" class="j-top">${region.value}</option>
                    </c:forEach>
                </select>
            </div>
        </c:forEach>
    </body>
</html>
